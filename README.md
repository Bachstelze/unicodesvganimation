# UnicodeSVGAnimation

Animate any unicode char or symbol your browser supports with SVG and CSS animations. I got the best character support with Chromium 69.

Inspired by some code snippets like https://codepen.io/davebitter/pen/XBGqro

![Alt text](https://gitlab.com/Bachstelze/unicodesvganimation/raw/master/medium_size_gifs/liberty.gif "Liberty")

## Usage

Change the Characters in the index.html as you wish and feel free to change the font, colors and delays.

```
    <text text-anchor="middle" x="50%" y="30%" dy=".35em">
     ☮ Change me ☄
    </text>
```

Save it as Gif to reduce the hardware workload.
For example with byzanz-record:

> byzanz-record --duration=6 --x=305 --y=169 --width=699 --height=245 lovely_levitate_black_gray.gif

If the file size matters you can try to convert it into a video:

> ffmpeg -i animated.gif -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" video.mp4

and load it again in your site:

```
 <video autoplay="autoplay" loop="loop" width="400" height="300">
   <source src="video.mp4" type="video/mp4" />
   <img src="video.gif" width="400" height="300" />
 </video>
 ```
 
## Symbols
 
 - http://xahlee.info/comp/unicode_animals.html
 - http://unicodefor.us/characters/
 - http://jrgraphix.net/r/Unicode/2600-26FF

## Archive

If you have generated some nice Gifs, I would appreciate a mate pull request.

## ToDo

Add more CSS animations like rotation or size morphing.
